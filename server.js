require('dotenv').config();
const express = require('express'); //import
const body_parser=require('body-parser');
const app=express();
const port=process.env.PORT || 3000;
const URL_BASE = process.env.URL_BASE;
const usersFile=require('./user.json');

app.listen(port, function(){
  console.log('Node JS escuchando en el puerto: '+port);
});

app.use(body_parser.json());
//operaciòn GET hola mundo
app.get (URL_BASE+'users',
  function(request,response){
    response.status(200).send(usersFile); //la respuesta (send) al final
});

//peticion GET a un unico usuario mediante ID (instancia)
app.get(URL_BASE+'users/:id',
  function(request,response){
    console.log('id: '+request.params.id);
    let respuesta=existsUser(request.params.id);
    response.status(200).send(respuesta);
});

//GET con query string
app.get(URL_BASE+'usersq',
  function(req,res){
    console.log(req.query.id);
    console.log(req.query.country);
    res.send({"msg":"GET con query"})
});

//peticion POST a usersFile
app.post(URL_BASE+'users',
  function(req,res){
    console.log('POST a users');
    let tam = usersFile.length;
    let new_user={
      "id_user":tam+1,
      "first_name":req.body.first_name,
      "last_name":req.body.last_name,
      "email":req.body.email,
      "password":req.body.password
    }
    console.log(new_user);
    usersFile.push(new_user);
    res.status(201).send({"msg":"usuario creado"});
});

app.put(URL_BASE + 'users/:id',
   function(req, res){
     console.log("PUT /techu/v1/users/:id");
     let idBuscar = req.params.id;
     let updateUser = req.body;
     for(i = 0; i < usersFile.length; i++) {
       console.log(usersFile[i].id_user);
       if(usersFile[i].id_user == idBuscar) {
         let new_user={
           "id_user":usersFile[i].id_user,
           "first_name":updateUser.first_name,
           "last_name":updateUser.last_name,
           "email":updateUser.email,
           "password":updateUser.password
         }
	        usersFile[i] = new_user;
          writeUserDataToFile(usersFile);
          res.status(200).send({"msg" : "Usuario actualizado correctamente.", new_user});
       }
     }
     res.status(404).send({"msg" : "Usuario no encontrado.", updateUser});
   });

//peticiòn DELETE a users (mediante su id)
app.delete(URL_BASE + 'users/:id',
   function(req, res){
     let idBuscar = req.params.id;
     for(i = 0; i < usersFile.length; i++) {
       console.log(usersFile[i].id_user);
       if(usersFile[i].id_user == idBuscar) {
  	     usersFile.splice(i, 1);
         res.status(200).send({"msg" : "Usuario eliminado correctamente."});
       }
     }
     res.status(404).send({"msg" : "Usuario no encontrado."});
});

// LOGIN - users.json
app.post(URL_BASE + 'login',
  function(request, response) {
    console.log("POST /apicol/v2/login");
    console.log(request.body.email);
    console.log(request.body.password);
    var user = request.body.email;
    var pass = request.body.password;
    for(us of usersFile) {
      if(us.email == user) {
        if(us.password == pass) {
          us.logged = true;
          writeUserDataToFile(usersFile);
          console.log("Login correcto!");
          response.status(200).send({"msg" : "Login correcto.", "idUsuario" : us.id, "logged" : "true"});
        } else {
          console.log("Login incorrecto.");
          response.status(401).send({"msg" : "Login incorrecto."});
        }
      }
    }
});

// LOGOUT - users.json
app.post(URL_BASE + 'logout',
  function(request, response) {
    console.log("POST /apicol/v2/logout");
    var userId = request.body.id_user;
    for(us of usersFile) {
      if(us.id_user == userId) {
        if(us.logged) {
          delete us.logged; // borramos propiedad 'logged'
          writeUserDataToFile(usersFile);
          console.log("Logout correcto!");
          response.status(200).send({"msg" : "Logout correcto.", "idUsuario" : us.id});
        } else {
          console.log("Logout incorrecto.");
          response.status(401).send({"msg" : "Logout incorrecto."});
        }
      }
      //us.logged = true //comentado debido a que aplica logged: true a todos los registros
    }
});

function writeUserDataToFile(data) {
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data, null, 2);//permite la escritura con formato
  fs.writeFile("./user.json", jsonUserData, "utf8",
   function(err) { //función manejadora para gestionar errores de escritura
     if(err) {
       console.log(err);
     } else {
       console.log("Datos escritos en 'users.json'.");
     }
   });
}

function existsUser(id){
  let pos = id-1;
  let respuesta = (usersFile[pos]==undefined)?{"msg":"usuario no existente"}:usersFile[pos];
  return respuesta;
}
